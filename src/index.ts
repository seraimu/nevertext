/**
 * NeverText
 * @description A text storage site.
 * @version v0.0.1
 * @author Seraimu
 * @license GPL-3.0
 */

import express from 'express';
import log4js from 'log4js';
import { Config } from './types';
import { getConfig } from './util/configUtil';
import { testDatabase } from './util/dbUtil';

// Import routers
import { router as newTextRouter } from './routers/newText';

// App information
const APP_VERSION = 'v0.0.1';

// Loading config
const config: Config = getConfig();

// Get the logger
log4js.configure({
  appenders: {
    console: { type: 'console' },
    logFile: {
      type: 'dateFile',
      filename: 'server.log',
      pattern: '.yyyy-MM-dd-hh',
      compress: true,
    },
  },
  categories: {
    default: { appenders: ['console', 'logFile'], level: config.logLevel },
  },
});

const serverLogger: log4js.Logger = log4js.getLogger('Server');
serverLogger.info(`NeverText ${APP_VERSION} (c) 2020 Seraimu.`);
serverLogger.info('Starting server...');

const stopServerByFatal = function stoppingServerByFatal(err: string) {
  serverLogger.fatal(`The server has stopped with a critical error.\n${err}`);
  process.exit(1);
};

const app: express.Application = express();

// Test page
app.get('/', (req, res) => {
  res.render('index.ejs', {
    meta: {
      title: 'Home',
    },
  });
});

// Routers
app.use('/new', newTextRouter);

// Listen server
const server = app.listen(config.httpPort, () => {
  testDatabase()
    .then((msg) => {
      serverLogger.info(msg);
      serverLogger.info(
        'Done! If you want to stop the server, press the Ctrl and C keys.'
      );
    })
    .catch((err) => {
      stopServerByFatal(err);
    });
});

/**
 * @function stopServer
 * @description Stopping server
 * @returns { void }
 */
const stopServer = function stoppingServer(): void {
  serverLogger.info('Stopping server...');
  // Close web server
  server.close((serverError) => {
    if (serverError) {
      serverLogger.error(
        `The server could not be stopped successfully.\n${serverError}`
      );
      process.exit(1);
    }
    log4js.shutdown((loggerError) => {
      if (loggerError) {
        serverLogger.error(
          `The server could not be stopped successfully.\n${loggerError}`
        );
        process.exit(1);
      }
      process.exit();
    });
  });
};

// Stopping events
process.on('SIGTERM', () => stopServer());
process.on('SIGINT', () => stopServer());

export { serverLogger };
