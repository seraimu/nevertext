import express from 'express';

const router: express.Router = express.Router();

router.get('/', (req, res) => {
  res.render('new.ejs', {
    meta: {
      title: 'New text',
    },
  });
});

export { router };
