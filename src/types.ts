interface Config {
  httpPort: number;
  logLevel: 'mark' | 'fatal' | 'error' | 'warn' | 'info' | 'debug' | 'trace';
  database: {
    host: string;
    port: number;
    user: string;
    password: string;
    dbName: string;
    connectionLimit: number;
  };
}

export { Config };
