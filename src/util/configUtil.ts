import fs from 'fs';
import { Config } from '../types';

/**
 * @function getConfig
 * @description Get the configuration file
 * @returns { Config }
 */
const getConfig = function getConfigData(): Config {
  // Returns a JSON parsed file
  return JSON.parse(fs.readFileSync('./config.json', { encoding: 'utf-8' }));
};

/**
 * @function saveConfig
 * @description Saving data to a config file
 * @param { Config } data Data to be stored in the configuration
 * @returns { void }
 */
const saveConfig = function saveConfigData(data: Config): void {
  // Saving to config.json
  fs.writeFileSync('./config.json', JSON.stringify(data), {
    encoding: 'utf-8',
  });
};

// Export functions
export { getConfig, saveConfig };
