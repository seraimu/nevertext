import mariadb from 'mariadb';
import log4js from 'log4js';
import { getConfig } from './configUtil';

const dbLogger: log4js.Logger = log4js.getLogger('Database');
const config = getConfig();

const dbPool = mariadb.createPool({
  host: config.database.host,
  port: config.database.port,
  user: config.database.user,
  password: config.database.password,
  database: config.database.dbName,
  connectionLimit: config.database.connectionLimit,
});

dbLogger.info('Created database pool.');

/**
 * @function getConnection
 * @description Get database connection
 * @returns { Promise<mariadb.PoolConnection> }
 */
const getConnection = function getDatabaseConnection(): Promise<mariadb.PoolConnection> {
  return dbPool.getConnection();
};

/**
 * @function testDatabase
 * @description Testing database connection
 * @returns { Promise<string> }
 */
const testDatabase = function testingDatabase(): Promise<string> {
  return new Promise((resolve, reject) => {
    getConnection()
      .then((conn) => {
        conn
          .query('SELECT 1')
          .then(() => resolve('Database testing has been completed.'))
          .catch((err) => reject(err));
      })
      .catch((err) => reject(err));
  });
};

export { getConnection, testDatabase };
